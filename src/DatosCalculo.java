
public class DatosCalculo {

	/**	Largo del producto en metros. */
	private double largo;
	/**ancho del producto en metros.*/
	private double ancho;
	/**Alto del producto en metros.*/
	private double alto;
	/**Peso del producto en metros.*/
	private double peso;
	/**Constante de cubicaje del producto*/
	private int cubicaje;
	/**Total calculo cubicaje.*/
	private double totalCubicaje;
	
	/**
	 * Constructor.
	 */
	public DatosCalculo(final double largo, final double ancho, final double alto, final double peso, final int cubicaje) {
		this.alto = alto;
		this.largo = largo;
		this.ancho = ancho;
		this.peso = peso;
		this.cubicaje = cubicaje;
		this.totalCubicaje = calculaCubicaje(this.largo, this.ancho, this.alto, this.cubicaje);
	}
	/**
	 * Constructor sin argumentos.
	 */
	public DatosCalculo() {
		
	}
	
	public double calculaCubicaje(final double largo, final double ancho, final double alto, final double cubicaje) {
		return largo*ancho*alto*cubicaje;
	}
	
	public double getTotalCubicaje() {
		return totalCubicaje;
	}
	public void setTotalCubicaje(double totalCubicaje) {
		this.totalCubicaje = totalCubicaje;
	}
	public double getLargo() {
		return largo;
	}
	public void setLargo(double largo) {
		this.largo = largo;
	}
	public double getAncho() {
		return ancho;
	}
	public void setAncho(double ancho) {
		this.ancho = ancho;
	}
	public double getAlto() {
		return alto;
	}
	public void setAlto(double alto) {
		this.alto = alto;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public int getCubicaje() {
		return cubicaje;
	}
	public void setCubicaje(int cubicaje) {
		this.cubicaje = cubicaje;
	}
	
	
}
