import java.util.HashMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.ibm.icu.math.BigDecimal;

public class CostosGanancias {

	protected Shell shell;
	private Text textTotalCubicaje;
	private Text textTotalPeso;
	private double peso;
	private double cubicaje;
	private Table table;
	private double elMayor;
	private static double PORCENTAJE_10 = 0.1;
	private static double PORCENTAJE_20 = 0.2;
	private static double PORCENTAJE_25 = 0.25;
	private static double PORCENTAJE_30 = 0.3;
	private static double PORCENTAJE_35 = 0.35;
	private static double PORCENTAJE_40 = 0.4;
	private static double PORCENTAJE_45 = 0.45;
	private static double PORCENTAJE_50 = 0.5;
	private static double PORCENTAJE_55 = 0.55;
	private static double PORCENTAJE_60 = 0.6;
	private static double PORCENTAJE_65 = 0.65;
	private static double PORCENTAJE_70 = 0.7;
	private static double PORCENTAJE_75 = 0.75;
	private static double PORCENTAJE_80 = 0.8;
	private static double PORCENTAJE_85 = 0.85;
	private static double PORCENTAJE_90 = 0.9;
	private static double PORCENTAJE_95 = 0.95;
	private static double PORCENTAJE_100 = 1.0;
	
	private static double PRECIO_DOLAR = 730.0;
	private static double BASE_FLETE_AEREO = 1.88;
	private static double BASE_FUEL = 0.3;
	private static double BASE_TRANSFER = 0.12;
	private double totalFleteAereo;
	private double totalFuel;
	private double totalTransfer;
	private static double HANDLING = 50.0;
	private static double AES = 25.0;
	private static double AWB = 25.0;
	private double desconsolidacion;
	private double costoDecimal;
	private BigDecimal costo;
	private HashMap<Integer, String> preciosVenta = new HashMap<Integer, String>();
	
	

//	/**
//	 * Launch the application.
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		try {
//			CostosGanancias window = new CostosGanancias();
//			window.open();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	public CostosGanancias(final DatosCalculo datos) {
		this.peso = datos.getPeso();
		this.cubicaje = datos.getTotalCubicaje();
		if(this.peso > this.cubicaje) {
			elMayor = peso;
		}else {
			elMayor = cubicaje;
		}
		this.desconsolidacion = 65450/PRECIO_DOLAR;
		this.totalFleteAereo = BASE_FLETE_AEREO*elMayor;
		this.totalFuel = BASE_FUEL*elMayor;
		this.totalTransfer = BASE_TRANSFER*elMayor;
		
		if(totalFleteAereo < 120) {
			this.totalFleteAereo = 120;
		}
		if(totalTransfer < 37) {
			this.totalTransfer = 37;
		}
		this.costoDecimal = totalFleteAereo+totalFuel+totalTransfer+desconsolidacion+AWB+AES+HANDLING;
		this.costo = new BigDecimal(this.costoDecimal);
		this.costo = this.costo.setScale(2,BigDecimal.ROUND_HALF_UP);
		llenaPreciosVenta();
	}
	public void llenaPreciosVenta() {
		int [] porcentajes = new int[] {10,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100};
		double valorPorcentual = 0;
		for(int elem: porcentajes) {
			switch(elem) {
			case 10:
				valorPorcentual = PORCENTAJE_10;
				break;
			case 20:
				valorPorcentual = PORCENTAJE_20;
				break;
			case 25:
				valorPorcentual = PORCENTAJE_25;
				break;
			case 30:
				valorPorcentual = PORCENTAJE_30;
				break;
			case 35:
				valorPorcentual = PORCENTAJE_35;
				break;
			case 40:
				valorPorcentual = PORCENTAJE_40;
				break;
			case 45:
				valorPorcentual = PORCENTAJE_45;
				break;
			case 50:
				valorPorcentual = PORCENTAJE_50;
				break;
			case 55:
				valorPorcentual = PORCENTAJE_55;
				break;
			case 60:
				valorPorcentual = PORCENTAJE_60;
				break;
			case 65:
				valorPorcentual = PORCENTAJE_65;
				break;
			case 70:
				valorPorcentual = PORCENTAJE_70;
				break;
			case 75:
				valorPorcentual = PORCENTAJE_75;
				break;
			case 80:
				valorPorcentual = PORCENTAJE_80;
				break;
			case 85:
				valorPorcentual = PORCENTAJE_85;
				break;
			case 90:
				valorPorcentual = PORCENTAJE_90;
				break;
			case 95:
				valorPorcentual = PORCENTAJE_95;
				break;
			case 100:
				valorPorcentual = PORCENTAJE_100;
				break;
			default:
				break;
			}
			double precio = this.costoDecimal*(valorPorcentual+1);
			BigDecimal bd = new BigDecimal(precio);
			bd = bd.setScale (2, BigDecimal.ROUND_HALF_UP);
			String precioVenta = String.valueOf(bd);
			preciosVenta.put(elem, precioVenta);
		}
	}
	/**
	 * Open the window.
	 * @wbp.parser.entryPoint
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(682, 390);
		shell.setText("Calculadora");
		
		Label lblCostoCubicaje = new Label(shell, SWT.NONE);
		lblCostoCubicaje.setBounds(10, 108, 110, 20);
		lblCostoCubicaje.setText("Costo Cubicaje:");
		
		Label lblCostoPeso = new Label(shell, SWT.NONE);
		lblCostoPeso.setBounds(10, 144, 110, 20);
		lblCostoPeso.setText("Costo Peso:");
		
		textTotalCubicaje = new Text(shell, SWT.BORDER);
		textTotalCubicaje.setEditable(false);
		textTotalCubicaje.setBounds(126, 108, 78, 26);
		textTotalCubicaje.append(String.valueOf(this.cubicaje));
		
		textTotalPeso = new Text(shell, SWT.BORDER);
		textTotalPeso.setEditable(false);
		textTotalPeso.setBounds(126, 144, 78, 26);
		textTotalPeso.append(String.valueOf(this.peso));
		
		table = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		table.setBounds(217, 10, 317, 323);
		
		TableColumn tblclmnPorcentajesDeMargen = new TableColumn(table, SWT.CENTER);
		tblclmnPorcentajesDeMargen.setWidth(90);
		tblclmnPorcentajesDeMargen.setText("% MARGEN");
		
		TableColumn tblclmnCosto = new TableColumn(table, SWT.NONE);
		tblclmnCosto.setWidth(72);
		tblclmnCosto.setText("COSTO");
		
		TableColumn tblclmnValorDeVenta = new TableColumn(table, SWT.NONE);
		tblclmnValorDeVenta.setWidth(130);
		tblclmnValorDeVenta.setText("VALOR DE VENTA");
		
		TableItem tableItem = new TableItem(table, SWT.NONE);
		tableItem.setText(new String[] {"10%",String.valueOf(this.costo),preciosVenta.get(10)});
		
		TableItem tableItem_1 = new TableItem(table, SWT.NONE);
		tableItem_1.setText("New TableItem");
		tableItem_1.setText(new String[] {"20%",String.valueOf(this.costo),preciosVenta.get(20)});
		
		TableItem tableItem_2 = new TableItem(table, SWT.NONE);
		tableItem_2.setText("New TableItem");
		tableItem_2.setText(new String[] {"25%",String.valueOf(this.costo),preciosVenta.get(25)});
		
		TableItem tableItem_3 = new TableItem(table, SWT.NONE);
		tableItem_3.setText("New TableItem");
		tableItem_3.setText(new String[] {"30%",String.valueOf(this.costo),preciosVenta.get(30)});
		
		TableItem tableItem_4 = new TableItem(table, SWT.NONE);
		tableItem_4.setText("New TableItem");
		tableItem_4.setText(new String[] {"35%",String.valueOf(this.costo),preciosVenta.get(35)});
		
		TableItem tableItem_5 = new TableItem(table, SWT.NONE);
		tableItem_5.setText("New TableItem");
		tableItem_5.setText(new String[] {"40%",String.valueOf(this.costo),preciosVenta.get(40)});
		
		TableItem tableItem_6 = new TableItem(table, SWT.NONE);
		tableItem_6.setText("New TableItem");
		tableItem_6.setText(new String[] {"45%",String.valueOf(this.costo),preciosVenta.get(45)});
		
		TableItem tableItem_7 = new TableItem(table, SWT.NONE);
		tableItem_7.setText("New TableItem");
		tableItem_7.setText(new String[] {"50%",String.valueOf(this.costo),preciosVenta.get(50)});
		
		TableItem tableItem_8 = new TableItem(table, SWT.NONE);
		tableItem_8.setText("New TableItem");
		tableItem_8.setText(new String[] {"55%",String.valueOf(this.costo),preciosVenta.get(55)});
		
		TableItem tableItem_9 = new TableItem(table, SWT.NONE);
		tableItem_9.setText("New TableItem");
		tableItem_9.setText(new String[] {"60%",String.valueOf(this.costo),preciosVenta.get(60)});
		
		TableItem tableItem_10 = new TableItem(table, SWT.NONE);
		tableItem_10.setText("New TableItem");
		tableItem_10.setText(new String[] {"65%",String.valueOf(this.costo),preciosVenta.get(65)});
		
		TableItem tableItem_11 = new TableItem(table, SWT.NONE);
		tableItem_11.setText("New TableItem");
		tableItem_11.setText(new String[] {"70%",String.valueOf(this.costo),preciosVenta.get(70)});
		
		TableItem tableItem_12 = new TableItem(table, SWT.NONE);
		tableItem_12.setText("New TableItem");
		tableItem_12.setText(new String[] {"75%",String.valueOf(this.costo),preciosVenta.get(75)});
		
		TableItem tableItem_13 = new TableItem(table, SWT.NONE);
		tableItem_13.setText("New TableItem");
		tableItem_13.setText(new String[] {"80%",String.valueOf(this.costo),preciosVenta.get(80)});
		
		TableItem tableItem_14 = new TableItem(table, SWT.NONE);
		tableItem_14.setText("New TableItem");
		tableItem_14.setText(new String[] {"85%",String.valueOf(this.costo),preciosVenta.get(85)});
		
		TableItem tableItem_15 = new TableItem(table, SWT.NONE);
		tableItem_15.setText("New TableItem");
		tableItem_15.setText(new String[] {"90%",String.valueOf(this.costo),preciosVenta.get(90)});
		
		TableItem tableItem_16 = new TableItem(table, SWT.NONE);
		tableItem_16.setText("New TableItem");
		tableItem_16.setText(new String[] {"95%",String.valueOf(this.costo),preciosVenta.get(95)});
		
		TableItem tableItem_17 = new TableItem(table, SWT.NONE);
		tableItem_17.setText("New TableItem");
		tableItem_17.setText(new String[] {"100%",String.valueOf(this.costo),preciosVenta.get(100)});

	}
}
