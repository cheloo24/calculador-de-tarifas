import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class Home {

	protected Shell shell;
	private Text textLargo;
	private Text textAlto;
	private Text textAncho;
	private Text textPeso;
	private Text textCubicaje;

	private double largo;
	private double alto;
	private double ancho;
	private double peso;
	private int cubicaje;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Home window = new Home();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(486, 340);
		shell.setText("Calculadora");

		Label lblLargo = new Label(shell, SWT.NONE);
		lblLargo.setBounds(10, 44, 50, 20);
		lblLargo.setText("Largo:");

		textLargo = new Text(shell, SWT.BORDER);
		textLargo.setBounds(66, 41, 40, 26);

		Label lblAlto = new Label(shell, SWT.NONE);
		lblAlto.setBounds(153, 44, 40, 20);
		lblAlto.setText("Alto:");

		textAlto = new Text(shell, SWT.BORDER);
		textAlto.setBounds(206, 41, 40, 26);

		Label lblAncho = new Label(shell, SWT.NONE);
		lblAncho.setBounds(293, 44, 50, 20);
		lblAncho.setText("Ancho:");

		textAncho = new Text(shell, SWT.BORDER);
		textAncho.setBounds(349, 41, 40, 26);

		Label lblDatosCubicacin = new Label(shell, SWT.NONE);
		lblDatosCubicacin.setBounds(10, 10, 221, 20);
		lblDatosCubicacin.setText("Datos Cubicaci\u00F3n");

		Label lblDatosPesokg = new Label(shell, SWT.NONE);
		lblDatosPesokg.setBounds(10, 147, 183, 20);
		lblDatosPesokg.setText("Datos Peso (Kg)");

		Label label = new Label(shell, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setBounds(10, 36, 417, 2);

		Label label_1 = new Label(shell, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_1.setBounds(10, 176, 417, 2);

		Label lblPesokg = new Label(shell, SWT.NONE);
		lblPesokg.setBounds(10, 184, 70, 20);
		lblPesokg.setText("Peso (Kg):");

		textPeso = new Text(shell, SWT.BORDER);
		textPeso.setBounds(81, 184, 78, 26);

		Button btnCalcular = new Button(shell, SWT.NONE);
		btnCalcular.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean flagValidacionesOk = true;
				String altoString = textAlto.getText();
				String anchoString = textAncho.getText();
				String largoString = textLargo.getText();
				String cubicajeString = textCubicaje.getText();
				String pesoString = textPeso.getText();

				if (altoString.isEmpty() || anchoString.isEmpty() || largoString.isEmpty() || cubicajeString.isEmpty()
						|| pesoString.isEmpty()) {
					flagValidacionesOk = false;
				}
				if (!flagValidacionesOk) {
					MessageBox messageBox = new MessageBox(shell, SWT.OK | SWT.ICON_WORKING);
					messageBox.setText("Error");
					messageBox.setMessage("Verifique los campos, no pueden estar vac�os.");
					messageBox.open();
				} else {
					try {
						alto = Double.parseDouble(altoString);
						ancho = Double.parseDouble(anchoString);
						largo = Double.parseDouble(largoString);
						peso = Double.parseDouble(pesoString);
						cubicaje = Integer.parseInt(cubicajeString);
					} catch (NumberFormatException exc) {
						MessageBox messageBox = new MessageBox(shell, SWT.OK | SWT.ICON_WORKING);
						messageBox.setText("Error");
						messageBox.setMessage("Verifique los campos, los decimales van con '.' en vez de ','!!");
						messageBox.open();
					}

					DatosCalculo datos = new DatosCalculo(largo, ancho, alto, peso, cubicaje);
					try {
						shell.close();
						CostosGanancias ventana = new CostosGanancias(datos);
						ventana.open();

					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		});
		btnCalcular.setBounds(99, 242, 235, 30);
		btnCalcular.setText("Calcular");

		Label lblm = new Label(shell, SWT.NONE);
		lblm.setBounds(112, 44, 35, 20);
		lblm.setText("(mt)");

		Label lblm_1 = new Label(shell, SWT.NONE);
		lblm_1.setBounds(252, 44, 35, 20);
		lblm_1.setText("(mt)");

		Label lblmt = new Label(shell, SWT.NONE);
		lblmt.setBounds(395, 44, 28, 20);
		lblmt.setText("(mt)");

		Label lblConstanteFlete = new Label(shell, SWT.NONE);
		lblConstanteFlete.setBounds(10, 89, 136, 20);
		lblConstanteFlete.setText("Constante Cubicaje:");

		textCubicaje = new Text(shell, SWT.BORDER);
		textCubicaje.setBounds(153, 89, 78, 26);

	}
}
